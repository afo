#!/usr/bin/python
# afo - automatized file opener.
# Copyright (C) 2010, 2011  Roman Zimbelmann <romanz@lavabit.com>
# This software is licensed under the terms of the GNU GPL version 3.
#
# -----------------------------------------------------
# Environment Variables are:
#
#  $f = the full path to the file name
#  $F = $f without the file extension
#  $e = the extension of $f only
#  $d = dirname($f)
#  $b = basename($f)
#  $B = basename($f) without the file extension
#  $args = the list of positional arguments passed to afo
#  $a0..$a999 = one specific argument passed to afo
#
# -----------------------------------------------------
# Configuration file:
#
# It's a yaml dictionary, in the form of:
# <regexp>: <programs>
#
# For example:
# py$: python2.7 $f
# (png|jpg)$: feh $f $args
#
# -----------------------------------------------------
# <regexp> is a regular expression that matches against this string:
# <mime-type>///<file-path>.  This way you can check for complicated
# relationships with a single regexp.  For example, this regular expression
# matches images (by mime-type) except SVG's: ^image.*(?<!svg)$
#
# The first regular expression that matches is used and its respective program
# is run.
#
# -----------------------------------------------------
# <program> is either a string, a list or a dictionary.  In case of a string,
# the command in the string is simply executed.  But sometimes it is necessary
# to open files in different ways.  For example, one way to compile it and one
# way to execute it:
#
# tex$:
#   compile: pdflatex $B
#   view: epdfview $B.pdf
# c$:
# - gcc $f -o /tmp/a.out
# - /tmp/a.out
# /home/me/my_project/:
# - cd /home/me/my_project; make
# - /home/me/my_project/my_executable
#
# You can then use the --ways option to specify which way to run it.
# For example, "afo --ways 0,1 test.c" would compile and run test.c.
#
# -----------------------------------------------------
# Sometimes you can't be sure whether a program is installed or not.
# In this case, you can specify multiple programs which will be run in succession
# until one ends with an exit code which is not 127 (sh's exit code when a
# command is not found.)  Examples:
#
# avi$:
#   normal:
#     - mplayer $f
#     - totem $f
#   fullscreen:
#     - mplayer -fs $f
#     - totem --fullscreen $f
# ^image:
# -
#   - sxiv $f
#   - feh $f
#
# -----------------------------------------------------
# If the first word in the program description starts with a "-", it will
# be interpreted as a set of additional afo options:
# avi$: -qf mplayer $f
# txt$: -p cat $f

__version__ = '1.0'
__license__ = 'GPL3'
__author__ = 'Roman Zimbelmann'

import mimetypes
import optparse
import os.path
import re
import subprocess
import sys
import time
try:
  import yaml
except ImportError:
  print("afo: error: python-yaml required.")

class AFO(object):
  def __init__(self, file, options=[], ways=[0], list_ways=False, args=[]):
    self.__dict__.update(locals())
    self.file = os.path.abspath(file)
    self.config = self._load_config()

    mimetypes.knownfiles.append(os.path.expanduser('~/.mime.types'))
    basename = os.path.basename(self.file)
    self.mimetype = mimetypes.MimeTypes().guess_type(basename, False)[0] or ""
    match_string = '%s///%s' % (self.mimetype, self.file)

    for entry, program in self.config.items():
      try:
        regex = re.compile(entry)
      except:
        print("afo: warning: Bad regexp: %s" % entry)
      else:
        if regex.search(match_string):
          return self._run(program)

    try:
      first_line = open(self.file).readline()
      if first_line[0:2] == '#!':
        program = first_line.strip()[2:]
        if program:
          return self._run(program + ' $f $@')
    except:
      pass

    print("afo: error: Unknown type")

  def _normalize_program_entry(self, program):
    if isinstance(program, str):
      return {'0': program}
    elif isinstance(program, dict):
      return dict(map((lambda v: (str(v[0]), v[1])), program.items()))
    elif isinstance(program, list):
      return dict(map((lambda v: (str(v[0]), v[1])), enumerate(program)))

  def _load_config(self):
    basedir = os.path.expanduser(os.getenv('XDG_CONFIG_PATH', '~/.config'))
    confpath = os.path.join(basedir, 'afo', 'config.yaml')
    try:
      return yaml.load(open(confpath))
    except Exception as e:
      print("afo: error: Failed to read config file:\n", e)
      return {}

  def _run(self, program):
    program = self._normalize_program_entry(program)
    if self.list_ways:
      print("\n".join(("%d: %s" % (n, line)) for n, line in program.items()))
    else:
      for way in self.ways:
        if way in program:
          if isinstance(program[way], list):
            self._shell(program[way])
          else:
            self._shell([program[way]])
        else:
          print("afo: warning: Unknown way `%s'" % str(way))

  def _generate_env(self, program):
    env = dict(os.environ)
    env.update({
        'f': self.file,
        'F': os.path.splitext(self.file)[0],
        'e': os.path.splitext(self.file)[1][1:],
        'd': os.path.dirname(self.file),
        'b': os.path.basename(self.file),
        'B': os.path.splitext(os.path.basename(self.file))[0],
        'm': self.mimetype,
        'args': " ".join(self.args),
        })
    for i, arg in enumerate(self.args):
      env["a%d" % (i+1)] = self.args[i]
    return env

  def _shell(self, commands):
    if set('vt') & set(self.options):
      print(command[0])

    if 't' not in self.options:
      for i, command in enumerate(commands):
        is_last = len(commands) is i + 1

        if command[0] == '-':
          options, command = command.split(' ', 1)
          self.options += options[1:]

        popen_kws = {'shell': True, 'env': self._generate_env(command)}
        if set('qf') & set(self.options):
          for key in ('stdout', 'stderr', 'stdin'):
            popen_kws[key] = open(os.devnull, 'a')
        p = subprocess.Popen(command, **popen_kws)

        if 'f' not in self.options:
          p.wait()

        elif not is_last:
          for i in range(100):
            if p.poll() is not None:
              break
            time.sleep(0.001)

        if p.poll() is not 127:
          is_last = True

        if is_last and 'w' in self.options:
          print("Press ENTER to continue")
          try: raw_input()
          except:  input()

        if is_last:
          break

  @staticmethod
  def get_parameters_from_argv(argv=None):
    class MoreOptions(optparse.Option):
      TYPES = optparse.Option.TYPES + ('list', )
      TYPE_CHECKER = dict(optparse.Option.TYPE_CHECKER,
          list=lambda _, __, value: value.split(','))

    p = optparse.OptionParser(option_class=MoreOptions,
        version='%prog ' + str(__version__),
        usage="%prog [options] path [-- args...]")
    p.add_option('-p', action='store_true', help='pipe output into a pager')
    p.add_option('-w', action='store_true',
        help='wait for a key press afterwards')
    p.add_option('-q', action='store_true', help='discard output')
    p.add_option('-t', action='store_true', help='test only')
    p.add_option('-v', action='store_true', help='be verbose')
    p.add_option('-f', action='store_true', help='fork process')
    p.add_option('--ways', type='list', default='0', metavar='N,M,..',
        help="open the file in what way(s)?")
    p.add_option('--list-ways', action='store_true',
        help="list all possible ways to run this file")
    keywords, args = p.parse_args(argv)
    if not len(args) > 1:
      p.print_help()
      raise SystemExit()
    opts = ''.join(f for f,v in keywords.__dict__.items() if len(f) == 1 and v)

    return {
        'options': opts,
        'file': args[1],
        'ways': keywords.ways,
        'list_ways': keywords.list_ways,
        'args': args[1:] }

if __name__ == '__main__':
  AFO(**AFO.get_parameters_from_argv(sys.argv))
